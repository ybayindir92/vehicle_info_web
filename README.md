# Vehicle information application

Vehicle information is a web application to display and search vehicles of owners.
Once the vehicle list is displayed, search and sort operations can be performed.


##### To start with locally...
'gradlew bootFullApplication' command is enough for running the application.  
This task installs the node.js, builds frontend and backend.  
First run can take a bit time because of node.js download and npm install.


##### Technologies
* Spring Boot
* Spring Data
* Spring Mvc
* Angular 6
* Bootstrap
* Restful Api
* REST-assured
* H2
* Gradle

