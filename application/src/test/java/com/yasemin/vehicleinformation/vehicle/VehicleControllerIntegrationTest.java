package com.yasemin.vehicleinformation.vehicle;

import com.yasemin.vehicleinformation.user.User;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static io.restassured.RestAssured.get;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class VehicleControllerIntegrationTest {

    @Autowired
    Environment environment;

    @MockBean
    VehicleRepository vehicleRepository;

    @Before
    public void setUp() {
        RestAssured.port = environment.getProperty("local.server.port", Integer.class);
    }

    @Test
    public void shouldReturnAllVehiclesWithoutUser() {
        // given
        BDDMockito.given(vehicleRepository.findAll()).willReturn(asList(
                Vehicle.builder().make("vw").model("golf").numberplate("123AAA").id(1L).user(new User()).build(),
                Vehicle.builder().make("ford").model("focus").numberplate("123BBB").id(2L).user(new User()).build()));

        // when
        final ValidatableResponse response = get("/cars").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", equalTo(2));
        response.assertThat().body("id", hasItems(1, 2));
        response.assertThat().body("make", hasItems("vw", "ford"));
        response.assertThat().body("model", hasItems("golf", "focus"));
        response.assertThat().body("numberplate", hasItems("123AAA", "123BBB"));
        response.assertThat().body("[0]", not(hasKey("user")));
        response.assertThat().body("[1]", not(hasKey("user")));
    }

    @Test
    public void shouldReturnNoContentWhenThereIsNoVehicle() {
        // given
        BDDMockito.given(vehicleRepository.findAll()).willReturn(emptyList());

        // when
        final ValidatableResponse response = get("/cars").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void shouldReturnAVehicle() {
        // given
        BDDMockito.given(vehicleRepository.findById(3L)).willReturn(
                Optional.of(Vehicle.builder().make("vw").model("golf").numberplate("123AAA").id(3L).build()));

        // when
        final ValidatableResponse response = get("/car/{id}", "3").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("id", equalTo(3));
        response.assertThat().body("make", equalTo("vw"));
        response.assertThat().body("model", equalTo("golf"));
        response.assertThat().body("numberplate", equalTo("123AAA"));
    }

    @Test
    public void shouldReturnNotFoundWhenVehicleIsNotExist() {
        // given
        BDDMockito.given(vehicleRepository.existsById(5L)).willReturn(false);

        // when
        final ValidatableResponse response = get("/car/{id}", "5").then();

        // then
        response.assertThat().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldReturnAllVehiclesWithUser() {
        // given
        BDDMockito.given(vehicleRepository.findAll()).willReturn(asList(
                Vehicle.builder().user(User.builder().name("kadri").id(3L).build()).build(),
                Vehicle.builder().user(User.builder().name("prit").id(5L).build()).build()));

        // when
        final ValidatableResponse response = get("/carsfull").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", equalTo(2));
        response.assertThat().body("user.name", hasItems("kadri", "prit"));
        response.assertThat().body("user.id", hasItems(3, 5));
    }

}
