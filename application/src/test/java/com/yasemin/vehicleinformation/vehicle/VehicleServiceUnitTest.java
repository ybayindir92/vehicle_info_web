package com.yasemin.vehicleinformation.vehicle;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class VehicleServiceUnitTest {

    @Mock
    private VehicleRepository vehicleRepository;

    @InjectMocks
    private VehicleService vehicleService;

    @Test
    public void shouldCallFindAllMethodOfRepository() {
        // when
        vehicleService.getAllVehicles();

        // then
        verify(vehicleRepository).findAll();
    }

    @Test
    public void shouldCallFindByIdMethodOfRepository() {
        // when
        vehicleService.getVehicleById(5L);

        // then
        verify(vehicleRepository).findById(5L);
    }

}
