package com.yasemin.vehicleinformation.user;

import com.yasemin.vehicleinformation.vehicle.Vehicle;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static io.restassured.RestAssured.get;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserControllerIntegrationTest {

    @Autowired
    Environment environment;

    @MockBean
    UserRepository userRepository;

    @Before
    public void setUp() {

        RestAssured.port = environment.getProperty("local.server.port", Integer.class);
    }

    @Test
    public void shouldReturnAllUsers() {
        // given
        BDDMockito.given(userRepository.findAll()).willReturn(asList(
                User.builder().name("john").id(1L).build(),
                User.builder().name("alex").id(2L).build()));

        // when
        final ValidatableResponse response = get("/users").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", is(2));
        response.assertThat().body("name", hasItems("john", "alex"));
        response.assertThat().body("id", hasItems(1, 2));
    }

    @Test
    public void shouldReturnNoContentWhenThereIsNoUser() {
        // given
        BDDMockito.given(userRepository.findAll()).willReturn(emptyList());

        // when
        final ValidatableResponse response = get("/users").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void shouldReturnAUser() {
        // given
        BDDMockito.given(userRepository.findById(3L)).willReturn(
                Optional.of(User.builder().name("yasemin").id(3L).build()));

        // when
        final ValidatableResponse response = get("/user/{id}", "3").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("name", equalTo("yasemin"));
        response.assertThat().body("id", equalTo(3));
    }

    @Test
    public void shouldReturnNotFoundWhenUserIsNotExist() {
        // given
        BDDMockito.given(userRepository.existsById(5L)).willReturn(false);

        // when
        final ValidatableResponse response = get("/user/{id}", "5").then();

        // then
        response.assertThat().statusCode(HttpStatus.NOT_FOUND.value());
    }

    @Test
    public void shouldReturnUserVehicles() {
        // given
        BDDMockito.given(userRepository.findById(7L)).willReturn(Optional.of(User.builder().vehicles(asList(
                Vehicle.builder().make("kia").model("sorento").numberplate("123ABC").id(1L).build(),
                Vehicle.builder().make("bmw").model("740").numberplate("456ASD").id(2L).build())).build()));

        // when
        final ValidatableResponse response = get("/user/{id}/cars", "7").then();

        // then
        response.assertThat().statusCode(HttpStatus.OK.value());
        response.assertThat().body("size()", equalTo(2));
        response.assertThat().body("id", hasItems(1, 2));
        response.assertThat().body("make", hasItems("kia", "bmw"));
        response.assertThat().body("model", hasItems("sorento", "740"));
        response.assertThat().body("numberplate", hasItems("123ABC", "456ASD"));
    }

    @Test
    public void shouldReturnNoContentIfUserHasNoCar() {
        // given
        BDDMockito.given(userRepository.findById(7L)).willReturn(
                Optional.of(User.builder().vehicles(emptyList()).build()));

        // when
        final ValidatableResponse response = get("/user/{id}/cars", "7").then();

        // then
        response.assertThat().statusCode(HttpStatus.NO_CONTENT.value());
    }

    @Test
    public void shouldReturnNotFoundWhenVehicleUserIdNotExist() {
        // given
        BDDMockito.given(userRepository.existsById(5L)).willReturn(false);

        // when
        final ValidatableResponse response = get("/user/{id}/cars", "5").then();

        // then
        response.assertThat().statusCode(HttpStatus.NOT_FOUND.value());
    }

}
