package com.yasemin.vehicleinformation.user;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceUnitTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    public void shouldCallFindAllMethodOfRepository() {
        // when
        userService.getAllUsers();

        // then
        verify(userRepository).findAll();
    }

    @Test
    public void shouldCallFindByIdMethodOfRepository() {
        // when
        userService.getUserById(5L);

        // then
        verify(userRepository).findById(5L);
    }

}
