package com.yasemin.vehicleinformation.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    List<User> getAllUsers() {
        return userRepository.findAll();
    }

    User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

}
