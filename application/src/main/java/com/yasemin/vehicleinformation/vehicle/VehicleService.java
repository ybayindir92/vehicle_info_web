package com.yasemin.vehicleinformation.vehicle;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class VehicleService {

    private final VehicleRepository vehicleRepository;

    List<Vehicle> getAllVehicles() {
        return vehicleRepository.findAll();
    }

    Vehicle getVehicleById(final Long id) {
        return vehicleRepository.findById(id).orElse(null);
    }

}
