package com.yasemin.vehicleinformation.vehicle;

import com.fasterxml.jackson.annotation.JsonView;
import com.yasemin.vehicleinformation.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vehicle_generator")
    @SequenceGenerator(name = "vehicle_generator", sequenceName = "vehicle_seq")
    @JsonView(VehicleSummaryJsonView.class)
    private Long id;

    @JsonView(VehicleSummaryJsonView.class)
    private String make;

    @JsonView(VehicleSummaryJsonView.class)
    private String model;

    @JsonView(VehicleSummaryJsonView.class)
    private String numberplate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

}
