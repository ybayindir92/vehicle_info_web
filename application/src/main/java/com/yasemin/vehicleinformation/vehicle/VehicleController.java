package com.yasemin.vehicleinformation.vehicle;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class VehicleController {

    private final VehicleService vehicleService;

    @GetMapping(value = "/cars")
    @JsonView(VehicleSummaryJsonView.class)
    public ResponseEntity<List<Vehicle>> getVehicles() {
        return getVehiclesWithUsers();
    }

    @GetMapping(value = "/carsfull")
    public ResponseEntity<List<Vehicle>> getVehiclesWithUsers() {
        final List<Vehicle> vehicles = vehicleService.getAllVehicles();
        if (vehicles.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(vehicles, HttpStatus.OK);
    }

    @GetMapping(value = "/car/{id}")
    public ResponseEntity<Vehicle> getVehicle(@PathVariable Long id) {
        final Vehicle vehicle = vehicleService.getVehicleById(id);
        if (vehicle == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(vehicle, HttpStatus.OK);
    }

}
