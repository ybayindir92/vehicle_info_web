package com.yasemin.vehicleinformation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VehicleInformationApplication {

    public static void main(String[] args) {
        SpringApplication.run(VehicleInformationApplication.class, args);
    }
}
