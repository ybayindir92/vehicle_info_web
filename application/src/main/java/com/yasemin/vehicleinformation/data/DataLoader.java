package com.yasemin.vehicleinformation.data;

import com.yasemin.vehicleinformation.user.User;
import com.yasemin.vehicleinformation.user.UserRepository;
import com.yasemin.vehicleinformation.vehicle.Vehicle;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@RequiredArgsConstructor
@Component
public class DataLoader implements ApplicationRunner {

    private final UserRepository userRepository;

    @Override
    public void run(final ApplicationArguments args) {

        final User userJarvekula = User.builder().name("Teet Järveküla").build();
        userJarvekula.setVehicles(asList(
                Vehicle.builder().make("Lada").model("2101").numberplate("123ASD").user(userJarvekula).build(),
                Vehicle.builder().make("Kia").model("Sorento").numberplate("534TTT").user(userJarvekula).build()));
        userRepository.save(userJarvekula);

        final User userPurk = User.builder().name("Pille Purk").build();
        userPurk.setVehicles(asList(
                Vehicle.builder().make("Skoda").model("Octavia").numberplate("999GLF").user(userPurk).build(),
                Vehicle.builder().make("Kia").model("Sorento").numberplate("555TFF").user(userPurk).build()));
        userRepository.save(userPurk);

        final User userKaal = User.builder().name("Mati Kaal").build();
        userKaal.setVehicles(asList(
                Vehicle.builder().make("Lada").model("2101").numberplate("445KKK").user(userKaal).build(),
                Vehicle.builder().make("Audi").model("A6").numberplate("997HHH").user(userKaal).build()));
        userRepository.save(userKaal);

        final User userKukk = User.builder().name("Külli Kukk").build();
        userKukk.setVehicles(asList(
                Vehicle.builder().make("BMW").model("760").numberplate("444RRR").user(userKukk).build(),
                Vehicle.builder().make("Audi").model("A6").numberplate("876OUI").user(userKukk).build()));
        userRepository.save(userKukk);

        final User userKruus = User.builder().name("Teet Kruus").build();
        userKruus.setVehicles(singletonList(
                Vehicle.builder().make("BMW").model("740").numberplate("112YUI").user(userKruus).build()));
        userRepository.save(userKruus);
    }

}
