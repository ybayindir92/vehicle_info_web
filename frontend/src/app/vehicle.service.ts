import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Vehicle} from "./model/vehicle";

@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private http: HttpClient) {
  }

  getVehicles() {
    return this.http.get<Vehicle[]>("/carsfull");
  }

}
