import {User} from "./user";

export class Vehicle {
  id: number;
  make: string;
  model: string;
  numberplate: string;
  user: User;
}
