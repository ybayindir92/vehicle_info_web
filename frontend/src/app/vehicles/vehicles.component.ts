import {Component, OnInit} from '@angular/core';
import {Vehicle} from "../model/vehicle";
import {VehicleService} from "../vehicle.service";

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  vehicles: Vehicle[];
  dtOptions: DataTables.Settings = {};

  constructor(private vehicleService: VehicleService) {
  }

  ngOnInit() {
    this.getAllVehicles();
    this.dtOptions = {
      pageLength: 10,
      lengthChange: false
    };
  }

  private getAllVehicles() {
    this.vehicleService.getVehicles().subscribe(data => {
      this.vehicles = data
    });
  }

}
